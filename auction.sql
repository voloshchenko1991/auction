CREATE DATABASE  IF NOT EXISTS `auction` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `auction`;
DROP TABLE IF EXISTS `buyer`;
CREATE TABLE `buyer` (
  `id` int(11) unsigned NOT NULL,
  `buyer_name` varchar(45) DEFAULT NULL,
  `buyer_phone` varchar(45) DEFAULT NULL,
  `buyer_address` varchar(45) DEFAULT NULL,
  `buyer_auction_number` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL,
  `product_name` varchar(45) DEFAULT NULL,
  `product_price` varchar(45) DEFAULT NULL,
  `product_category` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
DROP TABLE IF EXISTS `seller`;

CREATE TABLE `seller` (
  `id` int(11) unsigned NOT NULL,
  `seller_name` varchar(45) DEFAULT NULL,
  `seller_address` varchar(45) DEFAULT NULL,
  `seller_phone` varchar(45) DEFAULT NULL,
  `seller_license_number` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
DROP TABLE IF EXISTS `bid`;
CREATE TABLE `bid` (
  `id` int(11) unsigned NOT NULL,
  `bid_step` int(11) DEFAULT NULL,
  `current_bid` int(11) DEFAULT NULL,
  `current_leader_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_buyer_id` FOREIGN KEY (`id`) REFERENCES `buyer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_id` FOREIGN KEY (`id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_seller_id` FOREIGN KEY (`id`) REFERENCES `seller` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);